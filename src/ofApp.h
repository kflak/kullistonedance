#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxOsc.h"

class ofApp : public ofBaseApp{

    public:
        void setup();
        void update();
        void draw();

        void keyPressed(int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y );
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);

        ofVideoPlayer movie;
        ofFbo fbo;
        ofFbo movieFbo;
        ofTexture texture;
        // unsigned char* pixels;
        int nChannels;
        int vidWidth;
        int vidHeight;
        float history;
        float red;
        float green;
        float blue;
        float scale;
        ofxFloatSlider r;
        ofxFloatSlider g;
        ofxFloatSlider b;
        ofxFloatSlider h;
        ofxPanel gui;
        ofxOscSender sender;
        ofxOscReceiver receiver;
        

};
