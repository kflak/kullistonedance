#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

    ofBackground(0, 0, 0);
    ofSetVerticalSync(true);

    movie.load("kulliStoneDance.mp4");
    movie.play();

    gui.setup();
    gui.add(r.setup("red", 1, 0, 1));
    gui.add(g.setup("green", 1, 0, 1));
    gui.add(b.setup("blue", 1, 0, 1));
    gui.add(h.setup("history", 0.96, 0, 1));
    // gui.add(scale.setup("scale", 1, 0, 2));

    vidWidth = movie.getWidth();
    vidHeight = movie.getHeight();

    fbo.allocate(vidWidth, vidHeight);
    fbo.begin();
    ofBackground(0, 0, 0);
    fbo.end();

    history = 0.97;

    texture.allocate(vidWidth, vidHeight, GL_RGB);

    red = 1;
    green = 1;
    blue = 1;
    scale = (float)ofGetWidth() / (float)vidWidth;

    sender.setup("127.0.0.1", 57120);
    receiver.setup(8000);
}

//--------------------------------------------------------------
void ofApp::update(){
    while (receiver.hasWaitingMessages()) {
        ofxOscMessage m;
        receiver.getNextMessage(m);

        if(m.getAddress() == "/of/red"){
            red = m.getArgAsFloat(0);
        };
        if(m.getAddress() == "/of/green"){
            green = m.getArgAsFloat(0);
        };
        if(m.getAddress() == "/of/blue"){
            blue = m.getArgAsFloat(0);
        };
    };

    movie.update(); 
    if (movie.isFrameNew()){
        ofPixels pixels = movie.getPixels();
        nChannels = pixels.getNumChannels();


        fbo.begin();
        ofEnableAlphaBlending();
        float alpha = (1-h) * 255;
        // float alpha = (1-history) * 255;
        ofSetColor(0, 0, 0, alpha);
        ofFill();
        ofDrawRectangle(0, 0, vidWidth, vidHeight);
        ofDisableAlphaBlending();

        ofSetColor(0, 0, 0, history * 255);
        // ofSetHexColor(0x000000);

        for (int i = 4; i < vidWidth; i+=16){
            for (int j = 4; j < vidHeight; j+=16){
                int addr = (j * vidWidth + i + 2) * nChannels;
                unsigned char r = pixels[addr];
                float val = 1 - ((float)r / 255.0f);
                // ofDrawRectangle(i, j, val*18, val*18);
                ofDrawCircle(4+i, 4+j, val*12);
            }
        }
        fbo.end();

        for(int i=0; i<pixels.size(); i+=nChannels){
            pixels[i] *= r;
            pixels[i+1] *= g;
            pixels[i+2] *= b;
        }; 

        texture.loadData(pixels, GL_RGB);
    }
}

//--------------------------------------------------------------
void ofApp::draw(){

    ofPushMatrix();
    ofScale(scale);
    texture.draw(0, 0);
    fbo.draw(0, 0);
    ofPopMatrix();
    gui.draw();

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
